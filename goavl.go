// AVL Tree implementation in Golang
package goavl

type Node struct {
	Value  int
	Level  int
	Height int
	LTree  *Node
	RTree  *Node
}

func rotateRight(root *Node) *Node {
	node := root.LTree
	root.LTree = node.RTree
	node.RTree = root

	root.Height = max(height(root.LTree), height(root.RTree)) + 1
	node.Height = max(height(node.LTree), height(node.RTree)) + 1

	return node
}

func rotateLeft(root *Node) *Node {
	node := root.RTree
	root.RTree = node.LTree
	node.LTree = root

	root.Height = max(height(root.LTree), height(root.RTree)) + 1
	node.Height = max(height(node.LTree), height(node.RTree)) + 1

	return node
}

func doubleLRRotate(root *Node) *Node {
	root.LTree = rotateLeft(root.LTree)
	root = rotateRight(root)
	return root
}

func doubleRLRotate(root *Node) *Node {
	root.RTree = rotateRight(root.RTree)
	root = rotateLeft(root)
	return root
}

func rebalance(root *Node, value int) *Node {
	if height(root.LTree)-height(root.RTree) > 1 {
		if value < root.LTree.Value {
			root = rotateRight(root)
		} else {
			root = doubleLRRotate(root)
		}
		return root
	}
	if height(root.RTree)-height(root.LTree) > 1 {
		if value > root.RTree.Value {
			root = rotateLeft(root)
		} else {
			root = doubleRLRotate(root)
		}
		return root
	}
	return root
}

// Find finds 'value' in the AVL tree rooted in 'root'
func Find(root *Node, value int) *Node {
	if root == nil {
		return nil
	}
	if root.Value == value {
		return root
	}
	if value > root.Value {
		return Find(root.RTree, value)
	}
	if value < root.Value {
		return Find(root.LTree, value)
	}
	return root
}

// Insert inserts 'value' in the AVL tree rooted in 'root'
func Insert(root *Node, value int) *Node {
	if root == nil {
		root = &Node{value, 0, 0, nil, nil}
		root.Height = max(height(root.LTree), height(root.RTree)) + 1
		return root
	}

	if value < root.Value {
		root.LTree = Insert(root.LTree, value)
		root = rebalance(root, value)
	}

	if value > root.Value {
		root.RTree = Insert(root.RTree, value)
		root = rebalance(root, value)
	}

	root.Height = max(height(root.LTree), height(root.RTree)) + 1

	return root
}

// Delete deletes 'value' from the AVL tree rooted in 'root'
func Delete(root *Node, value int) *Node {
	if root == nil {
		return root
	}

	if x := Find(root, value); x == nil {
		return nil
	}

	if value > root.Value {
		root.RTree = Delete(root.RTree, value)
		//rebalance(root, value)
	} else if value < root.Value {
		root.LTree = Delete(root.LTree, value)
		//rebalance(root, value)
	} else { //if root.Value == value {

		// removing from leaf
		if root.RTree == nil && root.LTree == nil {
			return nil
		}

		// removing from half-leaf
		if root.LTree == nil && root.RTree != nil {
			return root.LTree

		} else if root.RTree == nil && root.LTree != nil {
			return root.LTree
		}

		// removing from inner
		if root.LTree != nil && root.RTree != nil {
			f := root
			aux := root.LTree
			for aux.RTree != nil {
				f = aux
				aux = aux.RTree
			}
			root.Value = aux.Value
			if aux == f.LTree {
				f.LTree = nil
			} else {
				f.RTree = nil
			}

		}

	}
	root.Height = max(height(root.LTree), height(root.RTree)) + 1
	balance := getBalance(root)

	if balance > 1 && getBalance(root.LTree) >= 0 {
		return rotateRight(root)
	}

	if balance > 1 && getBalance(root.LTree) < 0 {
		return doubleLRRotate(root)
	}

	if balance < -1 && getBalance(root.RTree) <= 0 {
		return rotateLeft(root)
	}

	if balance < -1 && getBalance(root.RTree) > 0 {
		return doubleRLRotate(root)
	}

	return root

}
