package goavl

import "fmt"

func max(x int, y int) int {
	if x > y {
		return x
	}
	return y
}

func height(root *Node) int {
	if root != nil {
		return root.Height
	}
	return -1
}

func getBalance(root *Node) int {
	if root == nil {
		return 0
	}
	return height(root.LTree) - height(root.RTree)
}

func level(root *Node, depth int) {
	if root == nil {
		return
	}
	level(root.LTree, depth+1)
	level(root.RTree, depth+1)
	root.Level = depth
}

func printTree(root *Node, depth int) {
	if root == nil {
		return
	}
	depth++

	printTree(root.RTree, depth)
	fmt.Println()
	for i := 0; i < depth; i++ {
		fmt.Print("\t")
	}
	fmt.Print(root.Value)

	printTree(root.LTree, depth)
}

// PrintTree print the Tree in the Stdout (usually the screen)
func PrintTree(root *Node) {
	level(root, 0)
	printTree(root, -1)
}
